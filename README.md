# Données Francil'IN

[Francil'IN](www.francilin.fr) recense les lieux d'inclusion numérique en Ile-de-France. Ce dépôt a vocation à héberger les données propres à ce recensement.

### Préparation des données

- exporter le tableur en ligne (Synology, URL privée) "Fichiers Draft Carto IN Idf" au format LibreOffice Calc
- commiter ce dernier sous `source/Fichiers Draft Carto IN Idf.ods`
- du fichier LibreOffice Calc :
  - exporter en CSV l'onglet `Referentiel Services` dans `source/referentiel_services.csv`
  - exporter en CSV l'onglet `SOURCE - fichier normalisé` dans `source/source_fichier_normalise.csv`, en prenant soin de ne pas exporter les colonnes tout à droite qui sont une aide destinée aux humains

### Envoi des données

tout envoi de données sur la branche master déclenche :

- un déploiement de ces nouvelles données sur : https://data.francilin.fr
- un redéploiement du site : https://carto.francilin.fr Ceci afin de regénérer toutes les pages statiques avec le contenu mis à jour.
